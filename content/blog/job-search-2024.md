+++
title = "Looking for a job"
date = "2024-11-07 12:30:00-08:00"
+++

Given recent events in the US, I'm now looking for work to help keep myself and my friends safe.

I have over 10 years of programming/hardware experience on microcontrollers, phones, and desktops/servers, spanning native and managed languages. I'm highly adaptable and quick to learn new domains, but have deep experience writing and profiling real-time/high-performance code, building and debugging thorny concurrent/multithreaded systems (hi Send/Sync!), and implementing audio/video signal processing (DSP). <!-- more --> I'm also experienced in communicating complex technical concepts through documentation and thorough bug reports. Some of my notable projects include:

- Debugging [analog video loss](https://nyanpasu64.gitlab.io/blog/wii-gbs-c-sync-loss/) on a game console, using an oscilloscope to discover noise bursts caused by room EMI (*[front-page HN!](https://news.ycombinator.com/item?id=40653352)*)
- Decoding, editing, and [reprogramming](https://codeberg.org/nyanpasu64/write-edid) computer monitor EDID information stored in I2C EEPROMs
- Debugging and patching X.org and the Linux kernel amdgpu driver's [screen resolution handling](https://gitlab.freedesktop.org/xorg/driver/xf86-video-amdgpu/-/issues/68) and [sleep-wake panics](https://gitlab.freedesktop.org/drm/amd/-/issues/2362)
- Building an [audio oscilloscope viewer](https://github.com/corrscope/corrscope/) using FFT-based waveform correlation and multicore parallelized rendering
- Setting up and administrating a home server using Docker and PostgreSQL, including a RSS reader, Git server, and debugging a [multithreaded race condition in music playback](https://github.com/MusicPlayerDaemon/MPD/issues/1900)

My open-source code is hosted on [GitHub](https://github.com/nyanpasu64/) and [Codeberg](https://codeberg.org/nyanpasu64). If you're interested in hiring me, drop me an email at nyanpasu256 `removethis` AT gmail DOT com, and I'll send you my resume. I'm based in the US (West Coast), and can work remotely and may be open to commuting or relocating.

🐈 Stay safe!
