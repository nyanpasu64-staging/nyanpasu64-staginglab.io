+++
title = "Update: CRT modelines and EDID editing"
date = "2023-10-20"

[taxonomies]
tags=["CRT"]
+++

I've released a major update to my article, "[A deep dive into CRT modelines and EDID editing](/blog/crt-modeline-cvt-interlacing/)". I've expanded on getting a VGA output using HDMI-to-VGA adapters (DACs), reading, writing, and editing EDID data on Windows, Linux, and Mac, and tuning modelines in CRU to improve geometry and sharpness. If you're interested in using CRT monitors on computers, be sure to check it out!

<!-- more -->
