+++
title = "Exploring the appearance of CRT televisions and monitors"
date = "2023-03-04"
updated = "2023-03-11"

[taxonomies]
tags=["CRT"]
+++

There's a recurring school of thought that the best way to experience the visuals of pre-HD console and PC games is to play them on period-accurate CRT displays. CRT images have a characteristic rich colorful look, with soft warm scanlines and crisp phosphors, as well as zero (added) latency and unmatched motion characteristics, all which make CRTs so beloved by retro gamers. Here we will explore how TV/monitor construction and operation create the various features making up the CRT look. <!-- more -->

## Still images

- 240p and 480i (15 KHz) console games were meant to be played on SDTV CRTs. Most consumer TVs have a relatively coarse dot pitch (white lines are actually composed of large dots or stripes of glowing RGB phosphors, spaced relatively far apart). This adds "texture" to game visuals, masking aliasing and blurry sprites/textures to a degree.

	- I think low-res LCDs actually have a similar horizontal texture to aperture grille (phosphor stripe) CRTs, but dot/slot mask (dot/slot-shaped phosphors) CRTs look quite different from LCDs when seen close up.

	- VGA CRT computer monitors have a much finer dot pitch (adjacent phosphors of the same color closer together), resulting in more phosphors of each color per screen height/width, even compared to bigger TVs. This makes them look closer to LCDs, since the color dots/stripes are so close together they can disappear at a normal viewing distance. Unfortunately this reduces the amount of "character" the phosphors add to 2D games, or even pre-HD (PS2/GC/Wii) 3D games.

- Unlike LCDs, CRTs have visible scanlines (lines of light, horizontal on wide monitors and vertical on rotated/tall arcade monitors) traced by the three electron beams. On a properly calibrated CRT, the three colors of scanline are vertically aligned; if your display has convergence errors, the scanlines and/or horizontal changes of brightness are misaligned between the three colors. On many (possibly not all) CRTs, each color's scanline gets taller (vertically wider) when the color is brighter, creating a "blooming" effect utilized by games. The scanlines are usually narrow enough on SDTVs that there's a visible black gap between adjacent lines of light in 240p games; modern emulators' "scanline" filters *add* dark gaps between rows of pixels.

	- I've heard the reason scanlines become taller as the image gets brighter, is because more of the cathode produces cathode rays (electron beams) as the voltage difference increases, resulting in a larger spot on the screen even after focusing.

- Most consoles output a horizontally soft image (or composite video blurs it horizontally). In 480i on a SDTV, adjacent scanlines on alternating fields (aka "frames") overlap vertically. Both effects blur the image and reduces high image frequencies, acting as "hardware-level anti-aliasing to every game for free".

	- Additionally, horizontal transitions between colors and black often cause the scanline to become narrower as it fades out, which is another characteristic of the CRT look. Dim pixels on black backgrounds can also produce a horizontally *shorter* scanline than bright pixels, due to CRT or composite video. ([Example of CRT scanlines and blur.](https://twitter.com/CRTpixels/status/1599513805717135360))

{{ thumb(src="metroid zero mission crt.jpg", alt="Metroid Zero Mission running on mGBA Wii, played on a 480i Trinitron CRT in 263-line 240p. (If it were 262.5-line 480i, the FPS counter would be closer to 59.94.)") }}

- One word you'll hear being thrown around when talking about CRTs is TVL, or television lines. This is a measure of how many vertical black and white lines (horizontal frequency) can be drawn in a width equal to the screen's height, before horizontal blurring turns it into a gray area (each scanline becomes gray), or dot pitch turns it into scrambled colorful aliasing.

{{ thumb(src="990px-EIA_Resolution_Chart_1956.svg.png", alt="EIA Resolution Chart with graduated vertical lines", title="EIA Resolution Chart with graduated vertical lines, used to measure the TVL of a monitor. ([Source](https://commons.wikimedia.org/wiki/File:EIA_Resolution_Chart_1956.svg))") }}

- Horizontal blur is caused by the CRT's video amplifier and the electron beam's spot size. A high-TVL TV with less horizontal blur has a smaller spot width, and if the spot is circular, that corresponds to a smaller spot height.

	- High-TVL TVs tend to have smaller spot widths and heights, resulting in thicker black scanline gaps. Low-TVL TVs tend to have wider light beams and thinner black gaps.

	- CRT TV users often prefer TVs with relatively high TVL (bigger gaps) to play 240p games with visible scanlines, and lower TVL (smaller gaps) to play 480i games or watch interlaced video. A modern high-resolution VGA monitor often has too high TVL and too thin electron beams (scanlines) for *either* use case.

	- (Sources: [Wikipedia](https://en.wikipedia.org/wiki/Television_lines), [AndyNumbers' Gaming Blog](https://andynumbers.wordpress.com/2016/07/21/lets-talk-about-tvl-count/), [Mod a consumer CRT television to make it high TVL](https://shmups.system11.org/viewtopic.php?t=67124))

- Unlike SDTVs, high-quality properly-focused VGA monitors have scanlines so narrow that drawing 240 scanlines across a 17 inch monitor produces taller *black gaps* than scanlines, even at *full brightness* (which produces maximum scanline height). This is very much not an attractive appearance for 240p games. One solution is to display each line twice in vertically-adjacent 480p scanlines (line doubling). Alternatively you can send a high-resolution (960p or above) video signal to the VGA CRT (producing no visible scanlines), and add emulated scanlines in a GPU shader or hardware scaler (ideally RetroTink-5X).

	 - Note that visible scanlines *boosts* high vertical frequencies up to (and to an extent, beyond) Nyquist, effectively acting as *alias-boosting* filters rather than anti-aliasing! **If you want phosphor dot texture and a smoothed on-screen image, which CRT TV/monitor you pick matters as much as playing on a CRT!**

	 - Both 240p and 480i games were meant to be played on SDTVs. 240p games were meant to be seen with scanline gaps, while 480i games were not.

	 - As an example of scanlines being "alias-boosting", my 17-inch VX720 VGA monitor has visible scanlines when running at 480p. They're beautiful to look at, and work well in fast-moving games with large objects like Super Monkey Ball 2 or Mario Kart Wii, or even some platformers like Battle for Bikini Bottom. But they boost aliasing to the point that if you're looking closely at fine detail during slower-paced gameplay, many 3D GC/Wii games have prominent jaggies that look worse than on a CRT TV or even a LCD display. In particular low-resolution textures being upscaled suffer greatly; for example, Wind Waker produces DOF distance blur by scaling the framebuffer to half resolution and showing it with bilinear interpolation, and Animal Crossing was ported from a N64 game and has N64-sized textures shown using nearest or bilinear interpolation. To soften up aliasing, when playing 480p games through my GBS-Control, I often configure it to upscale to 960p with vertical bilinear filtering (enable "line filter").

{{ thumb(src="monkey boat.jpg", alt="Photo of Monkey Boat from Super Monkey Ball 2 on a VGA CRT at 480p.", title="Photo of Monkey Boat from Super Monkey Ball 2, with deflicker manually disabled. This game looks beautiful with its rich water and lush environments. The fast motion and close-up camera (in gameplay) makes it work well with 480p scanlines.")}}

{{ thumb(src="animal crossing.jpg", alt="Photo of Animal Crossing at 480p, with text visible on-screen" title="Photo of Animal Crossing at 480p. Deflicker is off and geometry has sharp vertical color transitions aligned to scanlines. 3D model textures (animals and trees out the window) are low-res and upscaled on screen, resulting in visible artifacts.")}}

I'm not actually sure what type of monitor PC games from various eras (DOS through Windows XP) were meant to be seen on (eg. what dot pitch, beam sharpness, or maximum resolution); PC monitor resolutions have evolved substantially throughout the decades, while pre-HD consoles have universally targeted 15 KHz TVs. I'd appreciate input from someone who grew up in this era of PC gaming; contact me using my email address in the second line of [this link](https://gitlab.com/nyanpasu64/nyanpasu64.gitlab.io/-/commit/881ed202c95e45a41f8578797b19c0289ed006b3.patch).

- One thing I have heard is that EGA 15khz video modes, when shown on a VGA 31khz computer and monitor, are automatically line-doubled. So some people grew up playing DOS games with single clear scanlines, while other people remember the same games line-doubled! (Sources: [video](https://www.youtube.com/watch?v=zEqinYQ0RtI), [DOSBox Staging](https://github.com/dosbox-staging/dosbox-staging/issues/2283))

{{ thumb(src="osu crt.jpg", alt="An osu! background with an anime girl on pastel vaporwave colors, being displayed on my VGA monitor.", title="")}}

Anime-styled and vaporwave art looks beautiful on CRTs (both on TVs, low-res PC monitors, and hi-res like in this photo). Due to room light the black level is gray and saturation is reduced; this monitor had its scratched anti-glare filter removed using isopropyl alcohol, and glass stains removed using toothpaste, but the glass scratches remain. ([Image source.](https://osu.ppy.sh/community/contests/160))

## Interlacing

- Console games since the PS2/GameCube era primarily output a 480i video signal, where every other frame (technically field)'s scanlines are vertically offset by half a line, causing solid colors to appear "filled" without gaps between scanlines. However, if your eyes are tracking an object moving up/down by half a line per field, then the scanlines line up between fields and gaps reappear. This is because each scanline is only illuminated for a small fraction of a field, and by the time the next field appears and the scanlines have moved half a line up/down, your eye has moved that distance as well.

	- Additionally, when an object is moving vertically by half a line per field, half the vertical image detail disappears as well. Be glad that televisions didn't have vertical scanlines interlaced horizontally, and anything moving horizontally would lose half its horizontal detail to scanline gaps!

- In 480i video mode, if even and odd scanlines (appearing on even and odd fields) are different average brightnesses in a region of the image, the brightness will flicker between fields ("frames") when displayed on an CRT. To address this issue, many video games enable "deflicker", which blends together (on Wii) 3 adjacent scanlines when outputting the video (technically when copying an image to XFB). This blurs the image vertically but eliminates flickering of sharp horizontal edges or lines.

	 - Even at 480p output (which doesn't flicker), some GC/Wii games enable "deflicker" (vertical blur), which softens the image vertically. Nintendont and Wii USB loaders come with optional hacks which hook games to prevent them from turning on vertical blur.

	 - For some reason, in certain translucent effects (eg. fog, glow, or shadow), some GC/Wii games actually output alternating brightness per scanline (like vertical dithering) and rely on the deflicker filter to smooth it out to a uniform color. Running without deflicker enabled results in horizontal stripes of alternating colors, which I'm usually fine with (unless it's covering half the screen, like in the Wind Waker final boss), but some people find ugly.

{{ thumb(src="interlacing.jpg", alt="Photo of a YouTube video being shown from a laptop to a 480i Trinitron CRT, with combing artifacts visible from a 1/30 second camera exposure.", title="Interlaced video is especially tricky to photograph. 1/30 second exposure (two fields long) shows full vertical resolution, but creates combing artifacts in moving parts of the image. ([Video source](https://youtu.be/5YwjSUdx0wg?t=156))") }}

I'm not actually sure why the solid white color has scanlines as well; perhaps I set up my laptop's 480i modelines wrong? I don't recall it looking this bad in person. My [previous blog article](/blog/amdgpu-stray-crt-480i/) explores what can go wrong with an improperly interlaced video signal.

## Motion and latency

- Video signals, both analog VGA and digital HDMI (and probably DisplayPort as well, unsure if DP's MST alternates screens or interleaves them), always deliver the screen's contents from top to bottom over the duration of a frame. Displays (both CRTs and flat-panels) update from top to bottom as the image arrives into the screen. As a result, the bottom of a screen has nearly a full frame of extra latency compared to the top of the screen (unless you configure the game to turn off both vsync and triple buffering, resulting in tearing and the bottom of the screen receiving a newer video frame).

	- You can reduce the latency at the bottom of a screen by delivering pixels faster, and spending more time after each frame not delivering pixels ([HDMI Quick Frame Transport](https://forums.blurbusters.com/viewtopic.php?f=7&t=4064)). Unfortunately, neither retro game consoles nor CRTs (to my understanding) support displaying frames faster than normal.

- Different consoles, emulators, and games have different amounts of intrinsic latency between player inputs and sending an image to the screen. For example, on SNES, 240p Test Suite can respond on the *very next frame* after you press a button, but games can take multiple frames to process inputs and game logic (due to pipelined logic), resulting in several frames (on Super Mario World, 2 frames) where the result of a button press is not yet visible.

	- Many PC emulators have their own graphical latency of usually 1 to 2 frames, *on top* of game latency. However, they can reduce this added latency through techniques like hard GPU sync (avoiding CPU-GPU pipelining across multiple frames) and frame delay (polling input and running the emulator partway into the previous frame being shown, which increases the risk of stutters). Additionally they can use "runahead" to run the emulator for 2 or more frames and show the result of the *final* frame, then undo all but 1 frame of emulation when processing the next frame of user input (at the cost of added CPU usage).

	- Unusually, Dolphin Emulator advances the emulator and outputs frames based on the system clock, rather than advancing the emulator based on when the host GPU outputs frames. As a result, Dolphin works best on VRR LCD displays (and *not* CRTs like a Wii), and vsync causes unpredictable latency that increases after fast-forwarding and does not stabilize over time. I've [prototyped a system](https://github.com/dolphin-emu/dolphin/pull/12035) to automatically adjust emulation speed to maintain a constant latency, but it hasn't been merged.

If you're interested in game (not just display) latency tuning, there's an interesting overview at ["Latency mitigation strategies (by John Carmack)"](https://danluu.com/latency-mitigation/).

- LCDs and OLEDs update the state of each pixel once per frame (from top to bottom as the signal is transmitted from the video source), and pixels continue transmitting light for a full frame's duration until the next update arrives. (LCDs also have additional latency from an electrical signal to the image changing, because the liquid crystals respond slowly to electricity.) CRTs instead light up each point on the screen briefly once a frame, and the light output decays towards black (within <1ms on VGA monitors) until it's lit again a frame later. (Note that oscilloscopes and radar screens may use phosphors with far longer decay times: [Wikipedia "Standard phosphor types"](https://en.wikipedia.org/wiki/Phosphor#Standard_phosphor_types))

	- As a result, CRTs flicker (bad). But when your eye is tracking a moving object, the entire screen is not smeared by the distance of a single frame like in LCDs, resulting in a sharper image in motion (noticeable in video games and even web browser scrolling). There are techniques (black frame insertion, backlight strobing) to produce CRT-like motion properties (complete with flicker) in LCDs and OLED screens (more info at [Blur Busters "GtG versus MPRT"](https://blurbusters.com/gtg-versus-mprt-frequently-asked-questions-about-display-pixel-response/)).

	- Sadly, 30fps console games on CRT lose this beautiful fluid motion rendering, because they display the same video frame twice in a row. As a result, when your eye is tracking a moving object, the object (and screen) appears as a "double image", with the two copies 1⁄60 of a second (multiplied by your eye's movement speed) apart. The same "double image" appears (but with a shorter distance of 1⁄120 second apart, I don't know if it's perceptible) if you display a 60fps video at 120fps to avoid CRT flicker, or to feed 240p video into a 31-KHz-only monitor.

- One idea I have is "motion adaptive black-frame insertion" (similar to motion adaptive deinterlacing), where an (ideally) OLED display displays a static dim image for nonmoving sections of the image (to avoid flicker), and a bright strobed image for moving sections (to avoid eye tracking smearing). So a video filling part of the screen would flicker to ensure sharp motion, while the comments and browser UI filling the rest of the screen would remain static for reduced eyestrain. This could also be used to keep websites sharp while scrolling.

	- I'm not aware of any monitors which perform this trick, and I'm not sure if the 1ms or so of *added* latency to perform motion detection (my best guess, judging by the minimum latency of a RetroTINK-5X Pro or GBS-Control) would make this product unpalatable to gamers.

	- Another possible issue is that switching between uniform and strobed image brightness may be distracting, much like how I've heard 3D comb filters are distracting when the image starts and stops scrolling.

Visual demonstration of image persistence and CRT motion response at [UFO Test](https://www.testufo.com/mprt).

## Analog video

- RGB color (including VGA) and component video (YCbCr, green/blue/red RCA jacks) are theoretically lossless encodings.

- S-Video blurs color information (chroma) horizontally, but has theoretically unlimited brightness (luma) bandwidth horizontally.

- Composite video (single yellow RCA plug), as well as RF, encodes color as textured patterns (a 3.58 MHz sine wave in QAM with variable amplitude and phase) added to a black-and-white signal. This is a lossy encoding, since the QAM chroma signal's spectrum overlaps with high-frequency luma information.

	- TVs rely on various filters to separate color and brightness information. This results in horizontal color blur, some degree of brightness blur, and (with some video sources and TVs) horizontal changes in brightness being interpreted as color, or changes in color being interpreted as brightness dot crawl.

	- Some games were actually built to rely on composite video to blur color horizontally ([@CRTpixels "Symphony of the Night"](https://twitter.com/CRTpixels/status/1408451743214616587)), or even smooth out dithering ([@CRTpixels "Silent Hill"](https://twitter.com/CRTpixels/status/1454896073508413443)).

Note that I have less experience working with S-Video or composite video encoding, and generally run my game consoles in RGB or YCbCr (even though it's not period-correct for most pre-Dreamcast consoles).

{{ thumb(src="composite as luma.jpg", alt="Photo of SNES composite video being treated as luma and shown on a VGA CRT", title="Feeding 3-chip SNES composite video into the GBS-C's luma input, causing it to interpret chroma dots as luma patterns.")}}

To me, it looks like the SNES's BA6592F composite encoder generates hard-edged chroma stripes rather than a sine wave. However I don't have an oscilloscope to inspect the image in detail, and was unable to find information online. I do know the NES generates hard-edged chroma stripes ([source](https://www.nesdev.org/wiki/NTSC_video#Color_Phases)).

## Building a setup

As for my personal setup, I mostly play GC/Wii games (more than earlier generations of consoles). I run my Wii at 480p with a lossless component cable, through a GBS-Control scaler, into a Gateway VX720 17-inch Diamondtron VGA monitor. This monitor is useful because it's relatively compact compared to large TVs and fits on my computer desk, has excellent geometry, no audible whine (31 KHz is ultrasonic), and is sharp enough to be used as a computer monitor. The downside of using this monitor with console games is that the phosphors are too fine to add texture, the image (electron beam focusing, scanline height) is too sharp to act as antialiasing like a real TV (so I have to simulate it with bilinear scaling to 960p), and it cannot display 240p games properly unless upscaled like you were displaying on a LCD.

{{ thumb(src="wind waker title 480p.jpg", alt="Photo of The Wind Waker's title screen running on a VGA CRT", title="Photo of The Wind Waker running at 480p with visible scanlines on my VGA CRT. The title screen has no distance fog which would produce ugly aliasing. And scanline aliasing is actually *less* ugly in blurry photographs of the screen, than when looking at the razor-sharp scanlines in person.")}}

Sometimes I also plug this monitor into my PC (RX 570 -> [DP-to-VGA adapter](https://plugable.com/products/dpm-vgaf)) and use it as a second monitor, or for PC gaming. I bought this DP++-to-VGA dongle because I heard that DP-to-VGA dongles are less likely to have image quality problems than HDMI-to-VGA dongles. But I somewhat regret getting this, since most of my devices don't have DP ports so I can't use this dongle with any of my laptops (though my older laptops have native VGA output).

Additionally, my monitor doesn't broadcast EDID data when its front power switch is turned off (I power it down when not in use to reduce CRT wear). And when my DP++-to-VGA adapter's VGA output is unplugged or EDID is missing, the adapter identifies itself as an "unrecognized but present output" rather than no device at all, so powering on the monitor doesn't cause the PC to recognize the adapter as a newly plugged-in monitor. So for my computer to properly recognize my monitor, I have to first power on the monitor and plug in its VGA cable to the dongle, then plug the dongle into the DP port (and if it's already plugged in, squeeze the DP latch and unplug the dongle first).

{{ thumb(src="dual monitors kicad.jpg", alt="Photo of my CRT being used as a second monitor in KiCad, to show two PCBs at once.")}}

I used to have a 24-inch flat-screen Trinitron SDTV given to me by another person. Unfortunately, the screen geometry linearity was poor due to high deflection angles (objects were wider in the left and right of the screen, causing scrolling 2D games to warp and distort unnaturally), and the image had wide pincushion at the top of the screen. Additionally, the TV only ran at 15 KHz and did not support 480p from my Wii (480p improves fine detail in fast or vertical motion), and produced a high-pitched whine (which was painfully loud for the first 30 minutes after powering on). To block out the whine, I had to wear closed-back headphones and/or put layers of sound-muffling clothing around the TV's side vents, and set up a forced-air cooling fan to replace the obstructed air circulation. I ended up giving the TV away for free (after two potential buyers on Facebook Marketplace ghosted me).

Sadly it's now common for eBay and Marketplace sellers to offer CRT TVs and monitors at absurdly inflated prices, waiting for an uninformed or desperate buyer to pay hundreds of dollars for a PVM or average VGA monitor, or thousands for BVMs and widescreen PC monitors. Most of these listings sit for months on end with no buyers, only serving to clog up search results if you're looking for a CRT (whereas the good deals disappear fast). If you live in the US, I'd advise you to look out for the occasional "free TV" Craigslist/Marketplace listings (and mythical "free VGA monitor" offers), or see if any electronics recyclers will sell their CRTs to you at a reasonable price ($40 for a 17 inch VGA isn't a small amount of money to drop, but it's downright *generous* compared to the average eBay scalper).

### The HD era

> Were Wii games designed to be displayed on CRT or LCD?

I'd say a mix of both. The Wii offers both 480i SDTV output (for CRTs and LCDs, through the pack-in composite video cable) and 480p EDTV output (better for LCDs, but you had to buy a higher-quality component cable). Unfortunately 480p-compatible CRT TVs were rare and far between. Additionally 480p doesn't actually take advantage of the full resolution of 720p/1080p LCD TVs, resulting in a blurry image on-screen (but fortunately free of composite and interlacing artifacts, and deinterlacing latency on some TVs).

Did anyone commonly have 480p LCD EDTVs, or were they a rare transitional stage of television with little uptake? My family jumped straight from CRT (technically rear projection) to a 1080p HDTV. And some people online have called EDTVs awful; I suppose considering the black levels, color gamut, and response times of LCD computer monitors around 2006, people without EDTVs didn't miss out on much. I kinda want a CRT 480p EDTV like a Panasonic Tau, but they're quite rare and sell for hundreds of dollars.

Early Wii games were built to output in both 4:3 and 16:9, adjusting the camera width and HUD to match. I think System Menu actually looks better in 4:3, since the channel icons mostly add empty featureless left/right borders when expanded to 16:9. Some later games (NSMB Wii, Skyward Sword) only display in 16:9, adding a letterbox if the Wii is configured to play in 4:3. Interestingly, in NSMBW playing in 4:3 saves two seconds in one scrolling cutscene, because the game objects are actually loaded underneath the letter box, and appear "on screen" sooner, cutting the cutscene short ([video](https://www.youtube.com/watch?v=gkt8L3t1GEU)).

## Image gallery

{{ thumb(src="tsumikitty.jpg", alt="Anime catgirl Tsumikitty at 240p.")}}

{{ thumb(src="xp toolbars.jpg", alt="Photo of Windows XP on a VGA CRT, with 11 Internet Explorer toolbars, two search engine change popups, Bonzi Buddy, and DrWindows installed", title="I miss Windows XP.")}}

Moving my 1000hz gaming mouse cursor on a Windows XP machine hooked up to a CRT feels like the cursor is *anticipating* my thoughts and movements, not just responding to them.

{{ thumb(src="480p scanlines.jpg", alt="Close-up shot of 480p scanlines in Super Monkey Ball 2.", title="480p scanlines in Super Monkey Ball 2, showing minor convergence errors.")}}

{{ thumb(src="stray.jpg", alt="Photo of Stray running at 1280x1024 on a VGA CRT", title="Stray on CRT, vsynced to 85 Hz felt beautifully fluid and responsive, like the screen was a window into another world.")}}

{{ thumb(src="moomoo.jpg", alt="My cat Moomoo looking at Skyward Sword on my VGA CRT", title="hi moomoo")}}

{{ thumb(src="wind waker totg.jpg", alt="The Wind Waker, Tower of the Gods at 480p with deflicker off.")}}

{{ thumb(src="snes gc controller.jpg", alt="Photo of SNES controller connected to a Wii's GameCube ports playing SMW2", title="My SNES-to-GC controller project was cool, and I love this picture I took. Sadly the GC+ I used added over 20 ms of input latency due to a firmware bug (on top of Snes9x RX adding 1 frame of latency), making games unresponsive to play.")}}

*This blog post was edited on 2023-03-11 to fix information about TVL.*
